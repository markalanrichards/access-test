# Data Protection Access Tests

Have you ever wondered how many third parties have remote access to act as you on a website?

Let's test and find out some of them.

## Run it yourself

* Install node version 20
* If you have browser problems, try customizing the webdriver.io configuration for your installed browser `src/with-browser.js` - you may need to install geckodriver, chromdriver or similar first

```shell
npm ci
npm run run-checks -- example
npm run run-reports -- example/report
```

Create a new folder and try creating your own config folder for a site to check; if it has an existing login form you'll need to add css selectors, there are various demonstrations of this in the `tested` folder.

Then run the reports on your new reports folder to combine all yaml reports into one rendered html page.


## The test goal

Attempt to execute scripts and collect data by appending test code to third party JavaScript loaded in a page.

If a website has control of their website, then the third parties cannot load arbitrary code that the website owner has
not already tested.

## The how

We'll use an http proxy.

There are various enterprise options, but also free open source ones include Squid, mitmproy
and [Mockttp](https://github.com/httptoolkit/mockttp)

Mockttp offers a JavaScript api keeping this whole project in one language.

We'll passthrough all requests as normal, except those that look like third party JavaScript.

For third party JavaScript, append JavaScript to run the checks by attempting to spoof login forms or capture login form
values.

Then, using a web browser automation framework (webdriver.io) we can open web pages to check them and enter in spoofed
login form values to see if they can be captured.

### Setup

1. We create tests
    1. Add a script element to the web page that fetches the same script with a magic number.<br>If we receive a
       response with a magic number, then there is no SRI protection against the third party doing what they like.
    2. Add a script element that will add fake login forms, if they can add forms they ciphen credentials and request answers to 2FA or other additional security mechanisms..
    3. Add a script element to the web page that fetches the same script whenever a form field changes, with the change
     included in the url,<br>If a script can get past SRI, it seems likely it can capture form fields like usernames
     and passwords.  
2. We need to generate a magic number no one will ever use, for explanation purposes let's use `9a8b7c8d`
3. Using https://webdriver.io/docs/mocksandspies
    1. We intercept any JavaScript requests that do not include a magic number (like `9a8b7c8d`) and we add our test
       code to the end and return the JavaScript as normal otherwise
    2. We intercept any requests that include a magic number (like `9a8b7c8d`) and instead of proxying them, we fetch
       any test output that could go to a third party.

