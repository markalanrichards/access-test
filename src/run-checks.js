import {mkdir, readdir} from "node:fs/promises";

import {dirname} from "node:path"
import {runCheck} from "./run-check.js";

const run = async () => {
    const srcDir = process.argv[2];
    const configDir = srcDir + "/" + "config"
    const reportDir = srcDir + "/" + "report"
    const reportFilenames = (await readdir(configDir, {recursive: true})).filter(filename => filename.match(`.*.yaml$`))
    const mkdirsPromises = Promise.all(reportFilenames
        .map(filename => mkdir(reportDir + "/" + dirname(filename), {recursive: true})))
    await mkdirsPromises
    const finalPromise = reportFilenames.reduce(
        (promise, curr) => promise.then(() => runCheck(
            configDir + "/" + curr,
            reportDir + "/" + curr + "-report.yaml")),
        Promise.resolve()
    )
    await finalPromise
}
run()