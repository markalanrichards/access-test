import Handlebars from "handlebars";
const template = Handlebars.compile(`
<section>
<h2>Some stats</h2>
<section>
<h3>How many banking websites give these sites remote access as you?</h3>
<div class="chart-container" style="height:20vh">
  <canvas id="domainCount"></canvas>
</div>
</section>
<section>
<h3>How many banking websites give these companies remote access as you?</h3>

<div class="chart-container" style="height:20vh">
  <canvas id="hasAccessCount"></canvas>
</div>
</section>
<script>
    const domainsCount = {{{ domainsCount }}};
    const hasAccessCount = {{{ hasAccessCount }}};
    new Chart(document.getElementById('domainCount'), {
        type: 'bar',
        data: {
            labels: domainsCount.map(x => x.domain),
            datasets: [{
              label: 'These sites can be you',
              data: domainsCount.map(x => x.count),
              borderWidth: 1
            }]
          },
        options: {
            maintainAspectRatio: false,
          scales: {
            y: {
              beginAtZero: true
            }
          }
        },
      })
    new Chart(document.getElementById('hasAccessCount'), {
        type: 'bar',
        data: {
            labels: hasAccessCount.map(x => x.hasAccess),
            datasets: [{
              label: 'Company Count',
              data: hasAccessCount.map(x => x.count),
              borderWidth: 1
            }]
          },
        options: {
        maintainAspectRatio: false,
          scales: {
            y: {
              beginAtZero: true
            }
          }
        },
      })
      
</script>
</section>    
<section>
    <h2>A breakdown by UK banks</h2>
    <p>This is not exhaustive.</p>
    <p>Updated: 8th February 2025</p>
 {{#if table}}
<table class="theycanbeyou">
  <thead>
    <tr>
      <th scope="col">Website</th>
      <th scope="col">Provide remote access to</th>
    </tr>
  </thead>
  <tbody>
  {{#each table}}
  {{#if head}}
    <tr>
      <th scope="row" rowspan="{{ length }}">{{ site }}</th>
      <td>{{ head }}</td>
    </tr>
    {{#each tail}}                         
    <tr>
      <td>{{ . }}</td>
    </tr>                            
    {{/each}}
    {{/if}}
  {{/each}}
  </tbody>
</table>
{{/if}}
</section>
`);

export const renderCombinedReport = data =>  template(data)


