import {readdir, readFile, mkdir, writeFile} from 'node:fs/promises'
import {ownerFromUrl} from "./urlToOwner.js";
import {load as readYaml} from "js-yaml";
import {renderCombinedReport} from "./combined-report.js";
import {writePretty} from "./write-prettier.js";

import tldjs from 'tldjs'

const getDomain = tldjs.getDomain

function groupBy(list, keyGetter, valueGetter = x => x) {
    const map = {};
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map[key];
        if (!collection) {
            map[key] = [valueGetter(item)];
        } else {
            collection.push(valueGetter(item));
        }
    });
    return map;
}

const run = (async () => {
    const dir = process.argv[2];
    const files = await readdir(dir, {recursive: true});
    const reportFilenames = files.filter(filename => filename.match(`.*-report.yaml$`))
    let reportContentPromises = reportFilenames.map(reportFile => readFile(dir + "/" + reportFile, "utf8"));
    const promiseReportContents = Promise.all(reportContentPromises)
    const reportContents = await promiseReportContents;
    const reportObjects = reportContents.map(readYaml);
    let list = reportObjects.flatMap(reportObject => Object
        .entries(reportObject.results)
        .filter(x => x)
        .flatMap(([k, v]) => {
            const site = v.config.org
            const hasAccess = [...new Set(Object.entries(v.results)
                .filter(([k1, v1]) => v1.captured)
                .map(([k1, v1]) => ownerFromUrl(k1)))]
                .filter(thirdPartySite => thirdPartySite !== site)
            const domains = [...new Set(Object.entries(v.results)
                .filter(([k1, v1]) => v1.captured)
                .map(([k1, v1]) => getDomain(k1)))]
            return {site, hasAccess, domains}
        }));

    let groupedBySiteFlattened = Object.entries(groupBy(list, x => x.site)).reduce((acc, [k, v]) => {
        acc[k] = v.reduce((accV, currV) => {
            accV.hasAccess = [...new Set(accV.hasAccess.concat(currV.hasAccess))]
            accV.domains = [...new Set(accV.domains.concat(currV.domains))]
            return accV
        }, {hasAccess: [], domains: []})
        return acc;
    }, {})
    const domainsCount = Object.entries(Object.entries(groupedBySiteFlattened)
        .flatMap(([k, v]) => v.domains)
        .reduce((acc, curr) => {

            if (acc.hasOwnProperty(curr)) {
                acc[curr] = acc[curr] + 1
            } else {
                acc[curr] = 1
            }
            return acc
        }, {})).map(([domain, count]) => ({domain, count}))
    domainsCount.sort((a, b) => b.count - a.count)
    const hasAccessCount = Object.entries(Object.entries(groupedBySiteFlattened)
        .flatMap(([k, v]) => v.hasAccess)
        .reduce((acc, curr) => {
            if (acc.hasOwnProperty(curr)) {
                acc[curr] = acc[curr] + 1
            } else {
                acc[curr] = 1
            }
            return acc
        }, {})).map(([hasAccess, count]) => ({hasAccess, count}))
    hasAccessCount.sort((a, b) => b.count - a.count)
    console.log({hasAccessCount})
    const completeReportObject = Object.entries(
        groupBy(
            list,
            x => x["site"],
            x => x.hasAccess))
        .map(([k, v]) => {
            let hasAccess = [...new Set(v.flat())];
            hasAccess.sort()
            var head = hasAccess[0];
            var tail = hasAccess.slice(1);
            return {site: k, length: hasAccess.length, head: head, tail: tail};
        })
    completeReportObject.sort((a, b) => a.site.localeCompare(b.site))
    const templateData = {
        table: completeReportObject,
        domainsCount: JSON.stringify(domainsCount.slice(0, 5)),
        hasAccessCount: JSON.stringify(hasAccessCount.slice(0, 5)),
    }
    let completeReport = renderCombinedReport(templateData);
    await writePretty(dir + "/index.html", completeReport)
    const chartJs = await readFile("assets/chart.umd.js", "utf8")
    await mkdir(dir + "/assets", {recursive: true})
    await writeFile(dir + "/assets/chart.umd.js", chartJs, "utf8")

});
run();