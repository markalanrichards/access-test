import { writeFile } from "fs";
import { format } from "prettier";

export const writePretty = async (fileName, content) => {
  const prettiered = await format(content, { filepath: fileName });
  try {
    writeFile(fileName, prettiered, (err) => {
      if (err) {
        console.error(err);
        return;
      }
    });
  } catch (e) {
    console.error({ writePretty: e });
  }
};
