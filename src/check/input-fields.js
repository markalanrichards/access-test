export const renderEachScript = (jsUrlString, cssSelector, nonce) => {
  const sriCheck = new URL(jsUrlString);
  sriCheck.searchParams.set(`${nonce}-original-url`, jsUrlString);
  const base64url = btoa(sriCheck.toString());
  const cssSelectorB = btoa(cssSelector);
  return `
    {
        const scriptUrl = atob('${base64url}');
        const cssSelector = atob('${cssSelectorB}');
        waitForElm(cssSelector).then(element => {
            const value = element.value;
            if(value) {
                const scriptElement = document.createElement("script");
                const url = new URL(scriptUrl);
                url.searchParams.set("${nonce}-input-" + cssSelector, value);
                scriptElement.src = url.toString();
                document.querySelector("body").appendChild(scriptElement);    
            }
            element.addEventListener('change',  e => { 
                if( e.target.value) {
                  const scriptElement = document.createElement("script");
                  const url = new URL(scriptUrl);
                  url.searchParams.set("${nonce}-input-" + cssSelector, e.target.value);
                  scriptElement.src = url.toString();
                  document.querySelector("body").appendChild(scriptElement);    
                }
                
              }, false);
          })
    }
    `;
};
export const inputFieldCheck = {
  interceptCheck: (url, nonce) => {
    const reqUrl = new URL(url);
    const originalUrlString = reqUrl.searchParams.get(`${nonce}-original-url`);
    let result = {};
    reqUrl.searchParams.forEach((value, key) => {
      if (key.startsWith(`${nonce}-input-`)) {
        console.log(url)
        const inputKey = key.replace(new RegExp(`${nonce}-input-(.*)$`), "$1");
        const captured = {};
        captured[inputKey] = value;
        result[originalUrlString] = {
          captured: captured,
        };
      }
    });
    return result;
  },
  renderScript: (jsUrlString, cssSelectors, nonce) => {
    let js = "";
    for (const cssSelectorIndex in cssSelectors) {
      js =
        js +
        "\n" +
        renderEachScript(jsUrlString, cssSelectors[cssSelectorIndex], nonce);
    }
    return js;
  },
};
