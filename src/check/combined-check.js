import { sriCheck } from "./sri.js";
import { inputFieldCheck } from "./input-fields.js";
import { waitForElemScript } from "../wait-for-elem.js";
import { mergeDeep } from "../deepmerge.js";
import { fakeInputs } from "./fake-inputs.js";

export const combinedCheck = {
  interceptCheck: (url, nonce) =>
    mergeDeep(
      sriCheck.interceptCheck(url, nonce),
      inputFieldCheck.interceptCheck(url, nonce),
    ),
  renderScript: (requestJsUrlString, nonce, cssSelectors) =>
    `
{
  ${waitForElemScript}
  ${sriCheck.renderScript(requestJsUrlString, nonce)}
  ${inputFieldCheck.renderScript(requestJsUrlString, cssSelectors, nonce)}
  ${fakeInputs.renderScript(requestJsUrlString, nonce)}
}`,
};
