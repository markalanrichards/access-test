import { renderEachScript } from "./input-fields.js";
import { createHash } from "node:crypto";
const sha256 = (content) => createHash("sha256").update(content).digest("hex");
export const fakeInput = {
  name: "Spoofed form field",
  id: "spoofed-form-field",
  value: "spoofed-value",
};
const fakeInputJs = (jsUrlString, nonce, name, originalId, htmlId) => {
  const base64name = btoa(name);
  const base64id = btoa(htmlId);
  const base64OriginalId = btoa(originalId);
  return `
    {
        const waitForElm = (selector) => new Promise(resolve => {
            if (document.querySelector(selector)) {
              return resolve(document.querySelector(selector));
            }
            const observer = new MutationObserver(mutations => {
              if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
              }
            });
            observer.observe(document.body || document, {
              childList: true,
              subtree: true
            });
          });
        const name = atob('${base64name}');
        const id = atob('${base64id}');
        const originalId = atob('${base64OriginalId}');
        waitForElm('body').then(element => {
            if(document.querySelector('#' + id) == null) {
              const label = document.createElement("label");
              label.innerText = name;
              const inputElement = document.createElement("input");
              label.for = id
              inputElement.id = id;
              inputElement.name = originalId;
              element.prepend(inputElement);
              element.prepend(label);
            }
          })
    }
    ${renderEachScript(jsUrlString, "#" + htmlId, nonce)}
    `;
};
const renderFakeInput = (jsUrlString, nonce, name, id) =>
  fakeInputJs(jsUrlString, nonce, name, id, "x" + sha256(id + jsUrlString));
export const fakeInputs = {
  renderScript: (url, nonce) => {
    let output = "";
    output =
      output +
      "\n" +
      renderFakeInput(url, nonce, fakeInput.name, fakeInput.id) +
      "\n";
    return output;
  },
};
