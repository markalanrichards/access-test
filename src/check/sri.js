export const sriCheck = {
  interceptCheck: (url, nonce) => {
    const reqUrl = new URL(url);
    const originalUrlString = reqUrl.searchParams.get(`${nonce}-original-url`);
    if (originalUrlString) {
      const result = {};
      result[originalUrlString] = {
        "sri-failed": true,
      };
      return result;
    } else {
      return {};
    }
  },
  renderScript: (requestJsUrlString, nonce) => {
    const sriCheck = new URL(requestJsUrlString);
    sriCheck.searchParams.set(`${nonce}-original-url`, requestJsUrlString);
    const base64url = btoa(sriCheck.toString());
    return `
  waitForElm('body').then(body => {
    const base64Url = '${base64url}';
    const scriptElement = document.createElement("script");
    scriptElement.src = atob(base64Url);
    body.appendChild(scriptElement);    
  })
          `;
  },
};
