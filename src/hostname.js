export const hostNameSuffixToIgnoreForSameSite = (hostname) =>
  hostname.endsWith(".co.uk")
    ? hostname.replace(/.*[.?]([^.]+[.]co[.]uk)/, "$1")
    : hostname.replace(/.*[.?]([^.]+[.][^.]+)/, "$1");
