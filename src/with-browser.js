import { remote, Key } from "webdriverio";
export const withBrowser = async (proxyUrl, caFingerprint, callback) => {
  const browser = await remote({
    capabilities: {
      acceptInsecureCerts: true,
      browserName: "chrome",
      "goog:chromeOptions": {
        args: (process.env.CI ? ["headless", "disable-gpu"] : []).concat([
          `proxy-server=${proxyUrl}`,
          `ignore-certificate-errors-spki-list=${caFingerprint}`,
        ]),
      },
    },
    logLevel: "warn",
  });
  try {
    await callback(browser);
  } finally {
    await browser.deleteSession();
  }
};
