import { findFirstHeader, matchLowerKey } from "./find-header.js";
import { hostNameSuffixToIgnoreForSameSite } from "./hostname.js";
import { Buffer } from "node:buffer";
import { combinedCheck } from "./check/combined-check.js";
import { mergeDeep } from "./deepmerge.js";

const fakeInput = {
  name: "Spoofed form field",
  id: "spoofed-form-field",
  value: "spoofed-value",
};
export const createMockttpHandler = async (nonce, checkURLString, config) => {
  const login = config.login || [];
  const selectors = login.map((x) => x.selector).filter((x) => x);
  const checkURL = new URL(checkURLString);
  const ignoreHostnameSuffix = hostNameSuffixToIgnoreForSameSite(
    checkURL.hostname,
  );
  const requests = {};
  let resultsHolder = { results: {} };
  return {
    results: () => resultsHolder.results,
    interceptCheck: (reqURL) => {
      resultsHolder.results = mergeDeep(
        resultsHolder.results,
        combinedCheck.interceptCheck(reqURL, nonce),
      );
    },
    beforeRequest: async (req) => {
      if (req.url.includes(nonce)) {
        throw "This should not happen" + req.url;
      }
      requests[req.id] = req;
    },
    beforeResponse: async (resp) => {
      const req = requests[resp.id];
      const reqURL = new URL(req.url);
      const contentType =
        findFirstHeader(resp.headers, matchLowerKey("content-type")) || "";
      if (
        !reqURL.hostname.endsWith(ignoreHostnameSuffix) &&
        (reqURL.pathname.endsWith(".js") ||
          contentType.toLowerCase().includes("javascript"))
      ) {
        const result = {};
        result[req.url] = {};
        resultsHolder.results = mergeDeep(resultsHolder.results, result);
        const responseBody = await resp.body.getText();
        const appendedBody = combinedCheck.renderScript(
          req.url,
          nonce,
          selectors,
        );
        const replacedBody = responseBody + appendedBody;
        const replacedBuffer = Buffer(replacedBody, "utf8");
        return {
          body: replacedBuffer,
          headers: { ...resp.headers, "content-length": replacedBuffer.length },
          statusCode: resp.statusCode,
        };
      } else {
        return {
          rawBody: resp.body.buffer,
          headers: resp.headers,
          statusCode: resp.statusCode,
        };
      }
    },
  };
};
