export const matchLowerKey = (key) => (header) =>
  header.toLowerCase() === key.toLowerCase();

export const findFirstHeader = (headers, matcher) => {
  for (const headerKey in headers) {
    if (matcher(headerKey)) {
      return headers[headerKey];
    }
  }
};
