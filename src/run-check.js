import { createMockttpHandler } from "./mockttp-handler.js";
import { withBrowser } from "./with-browser.js";
import { withMockttpUrl } from "./with-mockttp.js";
import { delay } from "./delay.js";
import { readFile } from "fs/promises";
import { load as readYaml } from "js-yaml";
import { randomUUID } from "node:crypto";
import { Key } from "webdriverio";
import { createHash } from "node:crypto";
import { dumpYaml } from "./write-yaml.js";
import { writePretty } from "./write-prettier.js";
import { fakeInput } from "./check/fake-inputs.js";
function sha256(content) {
  return createHash("sha256").update(content).digest("hex");
}
async function selectorsToSelector(browser, selectors) {
  const root = selectors[0]
  let variable = await browser.$(root)
  for(let i=1;i<selectors.length;i++) {
    variable = await variable.shadow$(selectors[i])
  }
  console.log("Hello " + await variable.getText())
  return await variable;
}
export const runCheck = async (inputFileName, outputFileName) => {

  const fileName = inputFileName;
  const configFileContent = await readFile(fileName, "utf8");
  const config = readYaml(configFileContent);
  const timestamp = new Date().toISOString();
  const uuidv4 = () => randomUUID().toString();
  const runCheck = async (url, config) => {
    const nonce = uuidv4();
    const mockttpHandler = await createMockttpHandler(nonce, url, config);
    await withMockttpUrl(
        nonce,
        mockttpHandler.beforeRequest,
        mockttpHandler.beforeResponse,
        mockttpHandler.interceptCheck,
        async (mockttpUrl, caFingerprint) => {
          return withBrowser(mockttpUrl, caFingerprint, async (browser) => {
            await browser.url(url);
            await delay(20000);
            const cookieBannersSelectors =
                config["cookie-banners"] || [config["cookie-banner"]] || [];
            for (const cookieBannersSelectorsKey in cookieBannersSelectors) {
              try {
                const cookieBannerSelector =
                    cookieBannersSelectors[cookieBannersSelectorsKey];
                const cookieBannerElement = await browser.$(cookieBannerSelector);
                await cookieBannerElement.waitForClickable();
                await cookieBannerElement.click();
                await delay(10000);
              } catch (error) {}
            }

            const loginSteps = config["login"] || [];
            for (const loginStepI in loginSteps) {
              const loginStep = loginSteps[loginStepI];
              if (loginStep.switchToFrame) {
                browser.switchToFrame(0);
              }
              if (loginStep.selector) {

                const element = await browser.$(loginStep.selector);
                if (loginStep.value) {
                  await element.waitForClickable();
                  await element.click();
                  const valueToUse = loginStep.value;
                  if (valueToUse) {
                    await browser.keys(valueToUse.split("(?!^)"));
                    await browser.keys([Key.Tab]);
                  }
                }

                if (loginStep.selectByAttribute) {
                  await element.selectByAttribute(
                      loginStep.selectByAttribute.attribute,
                      loginStep.selectByAttribute.value,
                  );
                }
              }
              const elementToClickSelector = loginStep.click;
              if (elementToClickSelector) {
                const elementToClick = Array.isArray(elementToClickSelector) ? await selectorsToSelector(browser, elementToClickSelector) : await browser.$(elementToClickSelector);  
                try {
                  await elementToClick.waitForClickable();
                }
                catch {}

                await elementToClick.click();
              }
              
              await delay(5000);
            }
            const elements = await browser.$$("input[name=" + fakeInput.id + "]");
            for (const elementsIdx in elements) {
              try {
                const element = elements[elementsIdx];
                await element.scrollIntoView();
                await element.waitForClickable();
                await element.click();
                const valueToUse = fakeInput.value;
                if (valueToUse) {
                  await browser.keys(valueToUse.split("(?!^)"));
                  await browser.keys([Key.Tab]);
                }
                await delay(1000);
              } catch {}
            }
            await delay(10000);
          });
        },
    );

    let prettierResults = mockttpHandler.results();
    for (const url in prettierResults) {
      const currentResult = prettierResults[url];
      let captured = currentResult.captured || {};
      const distinctId = "#x" + sha256(fakeInput.id + url);
      if (distinctId in captured) {
        captured[fakeInput.id] = captured[distinctId];
        delete captured[distinctId];
      }
    }
    return prettierResults;
  };
  let results = {};
  for (const url in config) {
    let configElement = config[url];
    results[url] = {
      config: configElement,
      results: await runCheck(url, configElement)
    };
  }
  const yaml = dumpYaml({config, timestamp, results}).toString();
  await writePretty(outputFileName, yaml, (err) => {
    if (err) {
      console.error(err);
    }
  });
}