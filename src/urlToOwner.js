const MARK = "Mark";
const KIDS_HARMS = "KidsHarms";
const ownerToUrl = {
    "^https://kidsharms.com/.*": KIDS_HARMS,
    "^https://markalanrichards.com/.*": MARK,
    "^https://[^/]*amazonaws.com/.*": "Amazon",
    "^https://[^/]*token.awswaf.com/.*": "Amazon",
    "^https://[^/]*barclays.co.uk/.*": "Barclays",
    "^https://[^/]*callsign.com/.*": "Callsign",
    "^https://[^/]*google.com/.*": "Google",
    "^https://[^/]*googletagmanager.com/.*": "Google",
    "^https://[^/]*googleadservices.com/.*": "Google",
    "^https://[^/]*google-analytics.com/.*": "Google",
    "^https://[^/]*doubleclick.net/.*": "Google",
    "^https://[^/]*citibank.co.uk/.*": "Citibank",
    "^https://[^/]*citi.eu/.*": "Citibank",
    "^https://[^/]*ensighten.com/.*": "Cheq",
    "^https://[^/]*tiqcdn.com/.*": "Tealium",
    "^https://[^/]*twitter.com/.*": "Twitter",
    "^https://[^/]*liveperson.com/.*": "LivePerson",
    "^https://[^/]*liveperson.net/.*": "LivePerson",
    "^https://[^/]*lpsnmedia.net/.*": "LivePerson",
    "^https://[^/]*dashboard.co.uk/.*": "Lloyds",
    "^https://[^/]*appdynamics.com/.*": "AppDynamics",
    "^https://[^/]*facebook.net/.*": "Meta",
    "^https://[^/]*hsbc.co.uk/.*": "HSBC",
    "^https://[^/]*tealiumiq.com/.*": "Tealium",
    "^https://[^/]*bing.com/.*": "Microsoft",
    "^https://[^/]*lloydsbank.com/.*": "Lloyds Bank",
    "^https://[^/]*lloydsbank.co.uk/.*": "Lloyds Bank",
    "^https://[^/]*sitescdn.net/.*": "Yext",
    "^https://[^/]*optimizely.com/.*": "Optimizely",
    "^https://[^/]*episerver.net/.*": "Optimizely",
    "^https://[^/]*dynatrace.com/.*": "Dynatrace",
    "^https://[^/]*hotjar.com/.*": "Hotjar",
    "^https://[^/]*pcprotect.com/.*": "PC Protect",
    "^https://[^/]*onetrust.com/.*": "One Trust",
    "^https://[^/]*sub2tech.com/.*": "Sub2Tech",
    "^https://[^/]*licdn.com/.*": "Microsoft",
    "^https://[^/]*cookielaw.org/.*": "Cookie Law",
    "^https://[^/]*metrobank.co.uk/.*": "Metro Bank",
    "^https://[^/]*metrobankonline.co.uk/.*": "Metro Bank",
    "https://az416426.vo.msecnd.net/scripts/a/ai.*": "Microsoft",
    "^https://[^/]*polyfill.io/.*": "Jake Champion or Funnull",
    "^https://[^/]*adobedtm.com/.*": "Adobe",
    "^https://[^/]*adoberesources.net/.*": "Adobe",
    "^https://[^/]*demdex.net/.*": "Adobe",
    "^https://[^/]*natwest.com/.*": "Natwest",
    "^https://[^/]*santander.co.uk/.*": "Santander",
    "^https://[^/]*clicktale.net/.*": "Click Tale",
    "^https://[^/]*tsb.co.uk/.*": "TSB",
    "^https://[^/]*virginmoney.com/.*": "Virgin Money",
    "^https://[^/]*contentsquare.net/.*": "Contentsquare",
    "^https://[^/]*maxymiser.net/.*": "Oracle",
    "^https://[^/]*evidon.com/.*": "Crownpeak",
    "^https://[^/]*we-stats.com/.*": "BioCatch",
    "^https://[^/]*egain.cloud/.*": "eGain",
    "^https://[^/]*responsetap.com/.*": "Infinity Tracking",
    "https://www.splash-screen.net/97123/splash.js": "splash-screen.net",
    "^https://[^/]*nationwide.co.uk/.*": "Nationwide",
    "^https://[^/]*trustpilot.com/.*": "Trust Pilot",
    "^https://[^/]*tiktok.com/.*": "TikTok",
    "^https://[^/]*corvidae.ai/.*": "Corvidae",
    "^https://[^/]*adsrvr.org/.*": "The Trade Desk",
    "^https://[^/]*iesnare.com/.*": "TransUnion",
    "^https://[^/]*sc-static.net/.*": "Snapchat",
    "^https://[^/]*snapchat.com/.*": "Snapchat",
    "^https://[^/]*marinsm.com/.*": "Marin Software",
    "^https://[^/]*nextdoor.com/.*": "Nextdoor",
    "^https://[^/]*matomo.cloud/.*": "Matamo",
    "^https://[^/]*instana.io/.*": "Instana",
    "^https://[^/]*abtasty.com/.*": "AB Tasty",
    "^https://[^/]*decibelinsight.net/.*": "Medallia",
    "^https://[^/]*medallia.eu/.*": "Medallia",
    "^https://[^/]*gbqofs.com/.*": "GlassBox",
    "^https://[^/]*azureedge.net/.*": "Microsoft",
    "^https://[^/]*visualwebsiteoptimizer.com/.*": "Wingify",
    
}

const urlToOwner = Object.entries(ownerToUrl);
export const ownerFromUrl = url => {
    let found = urlToOwner.find(([k,v]) => url.toString().match(k));
    return found ? found[1] : url;
}
