import { dump } from "js-yaml";

export const dumpYaml = (yamlContent) =>
  dump(yamlContent, {
    sortKeys: true,
  });
