import {
  getLocal,
  generateCACertificate,
  generateSPKIFingerprint,
} from "mockttp";

export const withMockttpUrl = async (
  nonce,
  beforeRequest,
  beforeResponse,
  interceptCheck,
  callback,
) => {
  const https = await generateCACertificate();
  const local = getLocal({ https });
  try {
    local
      .forGet()
      .matching((request) => request.url.toString().includes(nonce))
      .thenCallback((request) => {
        interceptCheck(request.url);
        return {
          statusCode: 204,
        };
      });
    await local.forUnmatchedRequest().thenPassThrough({
      ignoreHostHttpsErrors: true,
      beforeRequest,
      beforeResponse,
    });
    await local.start();
    const caFingerprint = generateSPKIFingerprint(https.cert);
    const httpProxy = local.proxyEnv.HTTP_PROXY.toString();
    await callback(httpProxy.replace(/http:\/\/([\/]*)/, "$1"), caFingerprint);
  } finally {
    await local.stop();
  }
};
